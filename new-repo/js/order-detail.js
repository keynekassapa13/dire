$(document).ready(function() {

  $('.submitOrder').click(function() {
    goto('orderSuccessful');
  });

  $('#confirmationButton').click(function(){
    starting = $('#origin-input').val();
    destination = $('#destination-input').val();
    order_mode = $('input[name=type]:checked').val();
    sender = $('#sendername-input').val();
    senderphonum = $('#senderphonum-input').val();
    recip = $('#recipientname-input').val();
    recipphonum = $('#recipientphonum-input').val();

    /*
    if (starting == "" || destination == "" || sender == "" || senderphonum == "" || recip == "" || recipphonum == "") {
      $('.errormessage').show("fast");
      return false
    }
    */
    $("#order_start").text(starting);
    $("#starting_point").val(starting);

    $("#order_dest").text(destination);
    $("#destination_point").val(destination);

    $('#order_mode').text(": " + order_mode);


    $('#order_sender').text(sender + " - " + senderphonum);
    $('#order_recip').text(recip + " - " + recipphonum);

    $("#id_sender_name").val(sender);
    $("#id_sender_phonum").val(senderphonum);
    $("#id_recipient_name").val(recip);
    $("#id_recipient_phonum").val(recipphonum);

    //*********DISTANCE AND DURATION**********************//
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix({
        origins: [starting],
        destinations: [destination],
        travelMode: order_mode.toUpperCase(),
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
    }, function (response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
            var distance = response.rows[0].elements[0].distance.text;
            var duration = response.rows[0].elements[0].duration.text;
            $('#order_distance').text(": "+distance);
            $('#order_duration').text(": "+duration);
            result = 0;
            if (order_mode.toUpperCase() == 'WALKING' || order_mode.toUpperCase() == 'TRANSIT' ){
              result = parseFloat(distance) * 1.25;
              if (result < 4){result = 4;}
            }
            else {
              result = parseFloat(distance) * 1.95;
              if (result < 10.55){result = 10.55}
            }
            $('#order_price').text(': AUD $ '+result.toString());

        } else {
            alert("Unable to find the distance via road.");
        }
    });

  });

});

// This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function initMap() {
  var directionsService = new google.maps.DirectionsService();
  var directionsDisplay = new google.maps.DirectionsRenderer();
  var chicago = new google.maps.LatLng(-27.4698, 153.0251);
  var mapOptions = {
    zoom:12,
    center: chicago
  }
  var map = new google.maps.Map(document.getElementById('map'), mapOptions);
  directionsDisplay.setMap(map);

  var start = $('#origin-input').val();
  var end = $('#destination-input').val();
  var method = $('input[name=type]:checked').val();
  var request = {
    origin: start,
    destination: end,
    travelMode: method.toUpperCase()
  };

  directionsService.route(request, function(result, status) {
    if (status == 'OK') {
      directionsDisplay.setDirections(result);
    }
  });

  starting = $('#origin-input').val();
  destination = $('#destination-input').val();
  order_mode = $('input[name=type]:checked').val();

  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix({
      origins: [starting],
      destinations: [destination],
      travelMode: order_mode.toUpperCase(),
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
  }, function (response, status) {
      if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
          var distance = response.rows[0].elements[0].distance.text;
          var duration = response.rows[0].elements[0].duration.text;
          $('#order_distance').text("Distance : "+distance);
          $('#order_duration').text("Duration : "+duration);
          result = 0;
          if (order_mode.toUpperCase() == 'WALKING' || order_mode.toUpperCase() == 'TRANSIT' ){
            result = parseFloat(distance) * 1.25;
            if (result < 4){result = 4;}
          }
          else {
            result = parseFloat(distance) * 1.95;
            if (result < 10.55){result = 10.55}
          }
          $('#order_price').text('Price : AUD $ '+result.toString());

      } else {
          alert("Unable to find the distance via road.");
      }
  });
}

function calcRoute() {
  var directionsService = new google.maps.DirectionsService();
  var directionsDisplay = new google.maps.DirectionsRenderer();

  directionsService.route(request, function(result, status) {
    if (status == 'OK') {
      directionsDisplay.setDirections(result);
    }
  });
}
