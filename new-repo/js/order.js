$(document).ready(function() {

  $('.submitOrder').click(function() {
    goto('orderSuccessful');
  });

  $('#confirmationButton').click(function(){
    starting = $('#origin-input').val();
    destination = $('#destination-input').val();
    order_mode = $('input[name=type]:checked').val();
    sender = $('#sendername-input').val();
    senderphonum = $('#senderphonum-input').val();
    recip = $('#recipientname-input').val();
    recipphonum = $('#recipientphonum-input').val();
    storename = $('#storename').val();
    description = $('#description').val();
    totalprice = $('#totalprice').val();

    /*
    if (starting == "" || destination == "" || sender == "" || senderphonum == "" || recip == "" || recipphonum == "") {
      $('.errormessage').show("fast");
      return false
    }
    */

    if (storename == ''){

    }
    else {
      toggler('buy-hidden');
      $("#order_storename").text(": " + storename);
      $("#order_desc").text(": " + description);
      $("#order_estprice").text(": " + totalprice);
    }

    $("#order_start").text(starting);
    $("#starting_point").val(starting);

    $("#order_dest").text(destination);
    $("#destination_point").val(destination);

    $('#order_mode').text(": " + order_mode);


    $('#order_sender').text(sender + " - " + senderphonum);
    $('#order_recip').text(recip + " - " + recipphonum);

    $("#id_sender_name").val(sender);
    $("#id_sender_phonum").val(senderphonum);
    $("#id_recipient_name").val(recip);
    $("#id_recipient_phonum").val(recipphonum);

    //*********DISTANCE AND DURATION**********************//
    var service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix({
        origins: [starting],
        destinations: [destination],
        travelMode: order_mode.toUpperCase(),
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
    }, function (response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
            var distance = response.rows[0].elements[0].distance.text;
            var duration = response.rows[0].elements[0].duration.text;
            $('#order_distance').text(": "+distance);
            $('#order_duration').text(": "+duration);
            result = 0;
            if (order_mode.toUpperCase() == 'WALKING' || order_mode.toUpperCase() == 'TRANSIT' ){
              result = parseFloat(distance) * 1.25;
              if (result < 4){result = 4;}
            }
            else {
              result = parseFloat(distance) * 1.95;
              if (result < 10.55){result = 10.55}
            }
            $('#order_price').text(': AUD $ '+result.toString());

        } else {
            alert("Unable to find the distance via road.");
        }
    });

  });

});

function toggler(divId) {
    $("#" + divId).toggle();
}

// This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    mapTypeControl: false,
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13
  });

  new AutocompleteDirectionsHandler(map);
}

 /**
  * @constructor
 */
function AutocompleteDirectionsHandler(map) {
  this.map = map;
  this.originPlaceId = null;
  this.destinationPlaceId = null;
  this.travelMode = 'WALKING';
  var originInput = document.getElementById('origin-input');
  var destinationInput = document.getElementById('destination-input');
  var modeSelector = document.getElementById('mode-selector');
  this.directionsService = new google.maps.DirectionsService;
  this.directionsDisplay = new google.maps.DirectionsRenderer;
  this.directionsDisplay.setMap(map);

  var originAutocomplete = new google.maps.places.Autocomplete(
      originInput, {placeIdOnly: true});
  var destinationAutocomplete = new google.maps.places.Autocomplete(
      destinationInput, {placeIdOnly: true});

  this.setupClickListener('changemode-walking', 'WALKING');
  this.setupClickListener('changemode-transit', 'TRANSIT');
  this.setupClickListener('changemode-driving', 'DRIVING');

  this.setupPlaceChangedListener(originAutocomplete, 'ORIG');
  this.setupPlaceChangedListener(destinationAutocomplete, 'DEST');

  //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(originInput);
  //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(destinationInput);
  //this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(modeSelector);
}

// Sets a listener on a radio button to change the filter type on Places
// Autocomplete.
AutocompleteDirectionsHandler.prototype.setupClickListener = function(id, mode) {
  var radioButton = document.getElementById(id);
  var me = this;
  radioButton.addEventListener('click', function() {
    me.travelMode = mode;
    me.route();
  });
};

AutocompleteDirectionsHandler.prototype.setupPlaceChangedListener = function(autocomplete, mode) {
  var me = this;
  autocomplete.bindTo('bounds', this.map);
  autocomplete.addListener('place_changed', function() {
    var place = autocomplete.getPlace();
    if (!place.place_id) {
      window.alert("Please select an option from the dropdown list.");
      return;
    }
    if (mode === 'ORIG') {
      me.originPlaceId = place.place_id;
    } else {
      me.destinationPlaceId = place.place_id;
    }
    me.route();
  });

};

AutocompleteDirectionsHandler.prototype.route = function() {
  if (!this.originPlaceId || !this.destinationPlaceId) {
    return;
  }
  var me = this;

  this.directionsService.route({
    origin: {'placeId': this.originPlaceId},
    destination: {'placeId': this.destinationPlaceId},
    travelMode: this.travelMode
  }, function(response, status) {
    if (status === 'OK') {
      me.directionsDisplay.setDirections(response);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });

  //********************* DISTANCE AND DURATION **********************//
  /* var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix({
      origins: {'placeId': this.originPlaceId},
      destinations: {'placeId': this.destinationPlaceId},
      travelMode: this.travelMode,
      unitSystem: google.maps.UnitSystem.METRIC
  }, function (response, status) {
    alert("hey");
      if (status == google.maps.DistanceMatrixStatus.OK && response.rows[0].elements[0].status != "ZERO_RESULTS") {
          var distance = response.rows[0].elements[0].distance.text;
          var duration = response.rows[0].elements[0].duration.text;
          var dvDistance = document.getElementById("dvDistance");
          dvDistance.innerHTML = "";
          dvDistance.innerHTML += "Distance: " + distance + "<br />";
          dvDistance.innerHTML += "Duration:" + duration;

      } else {
          alert("Unable to find the distance via road.");
      }
  }); */
};

function initAutocomplete() {
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -33.8688, lng: 151.2195},
      zoom: 13,
      mapTypeId: 'roadmap'
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });
  }

function sendmessage() {
  alert('Your Message Has Been Sent! Thank You!');
  $('#user-firstname').val('');
  $('#user-lastname').val('');
  $('#user-email').val('');
  $('#user-message').val('');
}
