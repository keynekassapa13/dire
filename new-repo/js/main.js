var dataUser = [
    {
      "id": 1,
      "is_courier": 0,
      "gender": "Female",
      "first_name": "Helen",
      "last_name": "Nguyen",
      "username": "helen",
      "email": "hnguyen0@bloomberg.com",
      "phonum":"040212345678",
      "address":"12 Adelaide St",
      "password": "test123"
    }, {
      "id": 2,
      "is_courier": 0,
      "gender": "Male",
      "first_name": "Ahilmanp",
      "last_name": "Manto",
      "username": "manto",
      "email": "manto@bloomberg.com",
      "phonum":"040212345678",
      "address":"2 Dell St",
      "password": "test123"
    }, {
      "id": 3,
      "is_courier": 1,
      "gender": "Male",
      "first_name": "Carlos",
      "last_name": "Fowler",
      "username": "carlos",
      "email": "cfowler1@gnu.org",
      "phonum":"040212345678",
      "address":"1 Land St",
      "password": "test123"
    }
];

function getUser() {
  user = 0;
  if (getAllUrlParams().userid){
    for (data in dataUser) {
      if (dataUser[data]["id"] == getAllUrlParams().userid){
        user = dataUser[data];
      }
    }
  }
  return user
}

function checklogin(){
  if (getAllUrlParams().userid){
    for (data in dataUser) {
      if (dataUser[data]["id"] == getAllUrlParams().userid){
        user = dataUser[data];
      }
    }
  }
  else {
    window.location.href = "../index.html";
  }
}

function goto(page){
  window.location.href = page + ".html?userid=" + getAllUrlParams().userid;
}

function goto_home(){
  user = getUser();
  if (user == 0){
    goto('../index')
  }
  else
  {
    is_courier = user["is_courier"];
    if (is_courier == 1){
      goto('home-courier');
    }
    else {
      goto('home');
    }
  }
}

function getdata(attr){
  user = getUser();
  return user[attr]
}

function signup() {
  firstname = $('#user-firstname').val();
  lastname = $('#user-lastname').val();
  username = $('#user-username').val();
  email = $('#user-email').val();
  password = $('#user-password').val();
  phonum = $('#user-phonum').val();
  city = $('#user-city').val();
  if (firstname == '' || lastname == '' || email == '' || password == '' || phonum == '' || city == ''){

  }
  else {
  	window.location.href = "../index.html";
  	return false;
  }
}

function signup_courier() {
  firstname = $('#user-firstname').val();
  lastname = $('#user-lastname').val();
  username = $('#user-username').val();
  email = $('#user-email').val();
  password = $('#user-password').val();
  phonum = $('#user-phonum').val();
  city = $('#user-city').val();
  bsb = $('#user-bsb').val();
  accountnumber = $('#user-accountnumber').val();
  accountname = $('#user-accountname').val();
  if (firstname == '' || lastname == '' || email == '' || password == '' || phonum == '' || city == '' || accountnumber == '' || accountname == ''){

  }
  else {
  	window.location.href = "../index.html";
  	return false;
  }
}

function login(page) {
	flag = 0;
	userid = 0;
  is_courrier=0;
	username = $('.login-name').val();
	password = $('.login-password').val();
	for (data in dataUser) {
		if (dataUser[data]["username"] == username && dataUser[data]["password"] == password){
			flag = 1;
			userid = dataUser[data]["id"];
      is_courier = dataUser[data]["is_courier"];
		}
	}

	if (flag == 1){
		if (page == "index"){
      if (is_courier == 1) {
        window.location.href = "view/home-courier.html?userid="+userid.toString();
      }
      else {
        window.location.href = "view/home.html?userid="+userid.toString();
      }

		}
		else {
      if (is_courier == 1) {
        window.location.href = "home-courier.html?userid="+userid.toString();
      }
      else {
        window.location.href = "home.html?userid="+userid.toString();
      }
		}
	}
	else {
		$('.errormessage').show();
	}
	/*
	 */
	return false;
}

window.onload = function() {
	if (getAllUrlParams().message){
		alert(getAllUrlParams().message);
	}
}


function getAllUrlParams(url) {

  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

  // we'll store the parameters here
  var obj = {};

  // if query string exists
  if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];

    // split our query string into its component parts
    var arr = queryString.split('&');

    for (var i=0; i<arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');

      // in case params look like: list[]=thing1&list[]=thing2
      var paramNum = undefined;
      var paramName = a[0].replace(/\[\d*\]/, function(v) {
        paramNum = v.slice(1,-1);
        return '';
      });

      // set parameter value (use 'true' if empty)
      var paramValue = typeof(a[1])==='undefined' ? true : a[1];

      // (optional) keep case consistent
      paramName = paramName.toLowerCase();
      paramValue = paramValue.toLowerCase();

      // if parameter name already exists
      if (obj[paramName]) {
        // convert value to array (if still string)
        if (typeof obj[paramName] === 'string') {
          obj[paramName] = [obj[paramName]];
        }
        // if no array index number specified...
        if (typeof paramNum === 'undefined') {
          // put the value on the end of the array
          obj[paramName].push(paramValue);
        }
        // if array index number specified...
        else {
          // put the value at that index number
          obj[paramName][paramNum] = paramValue;
        }
      }
      // if param name doesn't exist yet, set it
      else {
        obj[paramName] = paramValue;
      }
    }
  }

  return obj;
}
