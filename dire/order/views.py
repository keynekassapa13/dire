from django.shortcuts import render
from django.http import HttpResponse
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Order
from django.contrib.auth.decorators import login_required

# Create your views here.

class IndexView(LoginRequiredMixin, generic.ListView):
    login_url = 'accounts:accountsLogin'
    template_name = 'order/index.html'
    slug = 'test'

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['some_thing'] = self.request.user.is_authenticated
        return data

    def get_queryset(self):
        return Order.objects.all()

class DetailView(generic.DetailView):
    model = Order
    template_name = 'order/detail.html'

@login_required
def CreateOrder(request):
    return render(request, 'order/order_form.html')

def SuccessfulOrder(request):
    if request.method=="POST":
        order = Order()
        order.userid = request.user
        
        order.starting_point = request.POST["starting_order"]
        order.destination_point = request.POST["destination_order"]

        order.sender_name = request.POST["sender_name"]
        order.sender_phonum = request.POST["sender_phonum"]
        order.recipient_name = request.POST["recipient_name"]
        order.recipient_phonum = request.POST["recipient_phonum"]

        order.status = 'ordered'

        order.save()
        return render(request, 'order/order_success.html')

class UpdateOrder(UpdateView):
    model = Order
    fields = ['starting_point','destination_point']

class DeleteOrder(DeleteView):
    model = Order
    success_url = reverse_lazy('order:orderIndex')

'''
def index(request):
    all_order = Order.objects.all()
    context = {
        'all_order' : all_order,
    }
    return render(request, 'order/index.html', context)

def detail(request, order_id):
    try:
        order = Order.objects.get(id=order_id)
    except Order.DoesNotExist:
        raise Http404('Order doesnt exist')
    return HttpResponse('<h1>Detail of ' + str(order_id) + '</h1>')
'''
