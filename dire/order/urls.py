
from django.urls import path
from . import views


app_name = "order"

urlpatterns = [
    # /order/
    path('', views.IndexView.as_view() , name='orderIndex'),

    # /order/:id
    path('<pk>', views.DetailView.as_view(), name='orderDetail'),

    # /order/create
    path('add/', views.CreateOrder, name='orderCreate'),

    # /order/success
    path('success/', views.SuccessfulOrder, name='orderSuccess'),

    # /order/:id/edit/
    path('<pk>/edit/', views.UpdateOrder.as_view(), name='orderUpdate'),

    # /order/:id/delete/
    path('<pk>/delete/', views.DeleteOrder.as_view(), name='orderDelete')
]
