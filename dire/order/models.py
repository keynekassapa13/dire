from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User

# Create your models here.

class Order(models.Model):
    starting_point = models.CharField(max_length=400)
    destination_point = models.CharField(max_length=400)
    sender_name = models.CharField(max_length=400)
    sender_phonum = models.CharField(max_length=400)
    recipient_name = models.CharField(max_length=400)
    recipient_phonum = models.CharField(max_length=400)
    #status ordered, on process, delivered
    status = models.CharField(max_length=400)
    courier = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name='couriers')
    userid = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='users')

    def get_absolute_url(self):
        return reverse('order:orderDetail', kwargs={'pk': self.pk})

    def __str__(self):
        return str(self.id)

    class Meta:
        ordering = ["id"]
