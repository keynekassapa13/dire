from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate, login
from django.contrib.auth.models import User
from django.db import models
from .models import UserProfile
from order.models import Order

# Create your views here.
def signupUser(request):
    return render(request, 'accounts/signup.html')
    '''form = UserCreationForm(request.POST)
    if form.is_valid():
        user = form.save()
        login(request, user)
        return redirect("home")'''

def createUser(request):
    if request.method == 'POST':
        usertype = 0
        firstname = request.POST["user-firstname"]
        lastname = request.POST["user-lastname"]
        email = request.POST["user-email"]
        username = request.POST["user-username"]
        password = request.POST["user-password"]
        phonum = request.POST["user-phonum"]
        city = request.POST["user-city"]
        reg_user = authenticate(request, username=username, password=password)
        if reg_user is not None:
            return HttpResponseRedirect(reverse('accounts:accountsSignup'))
        else:
            userdb = User.objects.create_user( username=username, email=email, password=password, first_name=firstname, last_name=lastname)
            
            print(userdb.id)
            profile = UserProfile()
            profile.user_id = userdb.id
            profile.phonum = phonum
            profile.city = city
            userdb.save()
            profile.save()
            return HttpResponseRedirect(reverse('accounts:accountshome'))

def signupcourier(request):
    return render(request, 'accounts/signup-courier.html')

def createcourier(request):
    if request.method == 'POST':
        usertype = True
        firstname = request.POST["user-firstname"]
        lastname = request.POST["user-lastname"]
        email = request.POST["user-email"]
        username = request.POST["user-username"]
        password = request.POST["user-password"]
        phonum = request.POST["user-phonum"]
        city = request.POST["user-city"]
        reg_user = authenticate(request, username=username, password=password)
        if reg_user is not None:
            return HttpResponseRedirect(reverse('accounts:accountsSignup'))
        else:
            userdb = User.objects.create_user( username=username, email=email, password=password, first_name=firstname, last_name=lastname)
            
            print(userdb.id)
            profile = UserProfile()
            profile.usertype = usertype
            profile.user_id = userdb.id
            profile.phonum = phonum
            profile.city = city
            userdb.save()
            profile.save()
            return HttpResponseRedirect(reverse('accounts:accountshome'))

def loginUser(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect("home")
    else:
        form = AuthenticationForm()
    return render(request, 'accounts/login.html', {'form' : form})

def logoutUser(request):
    logout(request)
    return redirect('home')

def profile(request):
    username = None
    context = {}
    if request.user.is_authenticated:
        user = request.user
        context = {'user':user}
    return render(request, 'accounts/profile.html', context)

def index(request):
    username = None
    context = {}
    if request.user.is_authenticated:
        user = request.user
        if user.userprofile.usertype:
            order = Order.objects.filter(status="ordered")
            myorder = user.couriers.all()
            context = {'user':user, 'order':order, 'myorder':myorder}
        else:
            order = user.users.all()
            context = {'user':user, 'order':order}
    return render(request, 'accounts/index.html', context)
