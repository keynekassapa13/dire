from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class UserProfile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	usertype = models.BooleanField(default=False)
	bio = models.CharField(max_length=1000)
	phonum = models.CharField(max_length=15)
	city = models.CharField(max_length=30)
	# 1 for courier, 0 for normal user

	def __str__(self):
		return self.user.username

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])