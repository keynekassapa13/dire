# Generated by Django 2.1.dev20180405145536 on 2018-05-06 08:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_userprofile_usertype'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='city',
            field=models.CharField(default=1, max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='phonum',
            field=models.CharField(default=1, max_length=15),
            preserve_default=False,
        ),
    ]
