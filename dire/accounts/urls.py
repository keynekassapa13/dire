from django.urls import path
from . import views

app_name = "accounts"

urlpatterns = [
	#sign up user
    path('signup', views.signupUser , name='accountsSignup'),

    #login user
    path('login', views.loginUser, name='accountsLogin'),

    #logout user
    path('logout', views.logoutUser, name='accountsLogout'),

    #create user
    path('create', views.createUser, name='createUser'),

    # create courier
    path('createcourier', views.createcourier, name='createcourier'),

    # profile page
    path('profile', views.profile, name='userprofile'),

    # signup courier
    path('courier/signup', views.signupcourier, name='signupcourier'),

    #home for registered user
    path('', views.index, name='accountshome')
]
